#!/bin/bash

data_file=$1;
to_address=$2;

printf "HELO smtp.cse.iitk.ac.in\n" > final_mail.txt;
printf "MAIL FROM:<arareddy@cse.iitk.ac.in>\n" >> final_mail.txt;
printf "RCPT TO:<%s>\n" "$to_address" >> final_mail.txt;
printf "DATA\n" >> final_mail.txt;
printf "From: Talla Aravind Reddy <arareddy@cse.iitk.ac.in>\n" >> final_mail.txt;
printf "To:<%s>\n" "$to_address" >> final_mail.txt;
printf "Date: $(date)\n" >> final_mail.txt;
printf "Suject: $3\n" >> final_mail.txt;
cat $data_file >> final_mail.txt;
printf "\n.\n" >> final_mail.txt;
printf "quit\n" >> final_mail.txt;
cat final_mail.txt | nc smtp.cse.iitk.ac.in 25;
