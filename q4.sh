#!/bin/bash

mail_start='HELO smtp.cse.iitk.ac.in
MAIL FROM:<arareddy@cse.iitk.ac.in>
RCPT TO:<EMAIL_OF_USER>
DATA
From: <arareddy@cse.iitk.ac.in>
To: <EMAIL_OF_USER>
Subject:80G LETTER
MIME-Version: 1.0
Content-Type: multipart/mixed;
	boundary="BoundaryText"
'

header_body='


--BoundaryText
Content-Type: text/plain;
'

header_attachment='


--BoundaryText
Content-Type: application/pdf; name = "80g.pdf"
Content-Transfer-Encoding: base64
Content-Disposition: attachment;
	filename=80g.pdf
'

echo "$mail_start" > complete_body.m4;
printf "Date: $(date)\n" >> complete_body.m4;
echo "$header_body" >> complete_body.m4;
cat 80g.mail.m4 >> complete_body.m4;
echo "$header_attachment" >> complete_body.m4;

awk '
BEGIN { FS=", "; }
{	
	print "define(NAME_OF_USER," $1 ")dnl" > "all_defines";
	print "define(EMAIL_OF_USER," $2 ")dnl" >> "all_defines";
	print "define(PAN_OF_USER," $3 ")dnl" >> "all_defines";
	print "define(DONATION_OF_USER," $4 ")dnl" >> "all_defines";

	system("m4 all_defines 80g.tex.m4 > attachment.tex");
	system("pdflatex attachment.tex");
	system("m4 all_defines complete_body.m4 > complete_body");

	system("cat complete_body > final_mail");
	system("(cat attachment.pdf | base64) >> final_mail");

	print "--BoundaryText--" >> "final_mail";
	print "\n.\n" >> "final_mail";
	print "quit" >> "final_mail";
	system("cat final_mail | nc smtp.cse.iitk.ac.in 25");

	close("complete_body.m4");
	close("all_defines");
	close("attachment.tex");
	close("final_mail");
}
END{}' as4.csv
